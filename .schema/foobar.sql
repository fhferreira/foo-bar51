/*
Navicat MySQL Data Transfer

Source Server         : 1. Local
Source Server Version : 50615
Source Host           : 127.0.0.1:3306
Source Database       : foobar

Target Server Type    : MYSQL
Target Server Version : 50615
File Encoding         : 65001

Date: 2015-11-17 12:21:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', 'Usuario 01', 'root', '$2y$10$yGY2x49EG9S.yaPFmYwKXeqMb.mO0ht4vj7QMIjng9ZMjNgIq06.6', '3RskaF2LdCUzCeSyvHHl99OVfEw7SNjtiEcKvqvc1BvPx9vkRUAQxDY8tchw', '2015-08-25 15:48:54', '2015-11-17 12:21:25');
