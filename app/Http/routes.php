<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [
    'uses' => 'AuthController@getLogin',
    'as' => 'login'
]);

Route::post('/', [
    'uses' => 'AuthController@postLogin',
    'as' => 'login'
]);

Route::group(['middleware' => 'auth'], function () {

    Route::get('inicio', [
        'uses' => 'UsersController@index',
        'as' => 'inicio'
    ]);

    Route::get('logout', [
        'uses' => 'AuthController@getLogout',
        'as' => 'logout'
    ]);

    Route::resource('usuario', 'UsersController');

});
