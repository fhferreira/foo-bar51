<!-- resources/views/auth/login.blade.php -->

<form method="POST" action="{!! URL::route('login') !!}">
    {!! csrf_field() !!}

    <div>
        Login
        <input type="text" name="login" value="{{ old('login') }}">
    </div>

    <div>
        Password
        <input type="password" name="password" id="password">
    </div>

    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>

    <div>
        <button type="submit">Login</button>
    </div>
</form>
